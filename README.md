# restapi-restassured-framework
restapi restassured framework with maveen
 

Below are the list of tools used to build the framework
![](img/FrameworkDetails.PNG)

**FrameWork structure**

   - 'reusable' folder contains endpoints call which is used by test cases, like: authentication, getBookingById, createBooking.
   - test cases are added in 'testcase' folder for below endpoints "partialUpdateBooking ,"deleteBooking" and "getBookingIds".
   - 'util' folder contains restassuredUtils and javaUtils required.
   - Test data are externalized  and are called from 'properties' folder like: Url.
   - Request Jsons are picked from 'testdata' folder
   - At the end of execution, extent reports gets generated and will be placed in report folder
   - testcases to be executed are listed in 'testng.xml'

![](img/FrameworkStructture.PNG)

**Test case Details**

     Each test case are independent of each other and can be run n number of times. new booking ids are created by
     create booking endpoint, Validated the updates of endpoint with  'getBookingDetailsById'

     - UpdateBooking class 
          1. Update booking with new booking id
          2. Update booking with existing booking id
          3. Update booking with invalid booking id 
     - DeleteBooking class 
          1. Delete booking with new booking id
          2. Deletee booking with existing booking id
          3. deletee booking with invalid booking id
     - PatchBooking class
          1. Partial update booking with new booking id
          2. Partial update booking with existing booking id
          3. Partial update booking for invlaid booking id
          4. Partial update of booking with put request


**How to execute test cases ?**

       Add the test cases to be executed in testng.xml
          - From IDE : Right click on testng.xml run as TestNG Suite
     

**Report**

![](img/ExtentReport.PNG)
*
