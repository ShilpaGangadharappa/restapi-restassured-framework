package booking.restapi_restassured_framework.testcases;

import org.testng.annotations.Test;

import booking.restapi_restassured_framework.reusable.ReusableAPIs;
import booking.restapi_restassured_framework.util.BaseClass;
import booking.restapi_restassured_framework.util.RestAssuredUtils;
import io.restassured.response.Response;

public class DeleteBooking extends BaseClass {
	
	RestAssuredUtils restAssuredUtils = new RestAssuredUtils();
	ReusableAPIs reusableAPIs = new ReusableAPIs();
    
	/**
     * In the below test case, We are creating new booking entry 
     * and deleting the new entry by passing id
     * validating by checking get booking by id should return not found
     */
	 @Test
		public void deleteBookingWithNewBookingID() {
		    log=extent.createTest("delete booking with new booking id");
		    log.info("To delete the details of the booking by new id");

		    String newBookingId = reusableAPIs.createNewBooking();
		    log.info("new booking id" + newBookingId);
		    
		    delete(newBookingId);
		    
		    log.info("Verifun booking details are deleted");
		    Response response = reusableAPIs.getBookingDetailsByIdAndReturnResponse(newBookingId);
		    response.then().assertThat().statusCode(404);

		}
		
	 /**
	     * In the below test case, We are passing existing booking entry 
	     * and deleting the new entry by passing id
	     * validating by checking get booking by id should return not found
	     */
	 
		@Test
		public void deleteBookingWithExistingBookingID() {
		    log=extent.createTest("deletee booking with existing booking id");
		    log.info("To delete the details of the booking by existing id");
	        String id = "4";
	        
	        delete(id);

	        log.info("Verifun booking details are deleted");
		    Response response = reusableAPIs.getBookingDetailsByIdAndReturnResponse(id);
		    response.then().assertThat().statusCode(404);
		}
		
		
		/**
	     * In the below test case, We are passing invalid booking id and trying to delete 
	     * validating by checking response should be not found
	     */
		@Test(priority = 1)
		public void deleteBookingWithInvalidBookingID() {
		    log=extent.createTest("deletee booking with invalid booking id");
		    log.info("To delete the details of the booking by invlaid id");
	        String invalidBookingId = "39999";
	        
	        String newAuthToken = reusableAPIs.getAuthToken();
	        log.info("auth token" + newAuthToken);
		    String cookieValue = "token="+newAuthToken;
		    
		    log.info("Validate Booking id not found");
			Response response = restAssuredUtils.deleteRequestwithHeaders(requestSpec,cookieValue, "/booking/"+invalidBookingId);
			response.then().assertThat().statusCode(405);		
		}
		
		public String delete(String bookingid) {
			
			String newAuthToken = reusableAPIs.getAuthToken();
		    log.info("auth token" + newAuthToken);
		    String cookieValue = "token="+newAuthToken;
		    
			Response response = restAssuredUtils.deleteRequestwithHeaders(requestSpec,cookieValue, "/booking/"+bookingid);
			response.then().assertThat().statusCode(201);		
			log.info("Booking deatils after partial update by passing Booking ID :" + response.asString());
			
			return response.asString();
		}
	
}
