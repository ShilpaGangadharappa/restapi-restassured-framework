package booking.restapi_restassured_framework.testcases;


import org.testng.Assert;
import org.testng.annotations.Test;

import booking.restapi_restassured_framework.reusable.ReusableAPIs;
import booking.restapi_restassured_framework.util.BaseClass;
import booking.restapi_restassured_framework.util.RestAssuredUtils;
import io.restassured.response.Response;

public class UpdateBooking  extends BaseClass {
	
	RestAssuredUtils restAssuredUtils = new RestAssuredUtils();
	String updateBookingJson = System.getProperty("user.dir") + "\\testdata\\updateBooking.json";
	ReusableAPIs reusableAPIs = new ReusableAPIs();
    
	/**
     * In the below test case, We are creating new booking entry 
     * and updating the new entry by passing id
     * validating by checking get booking by id should return updated values
     */
	
	 @Test(priority = 1)
		public void updateBookingWithNewBookingID() {
		    log=extent.createTest("update booking with new booking id");
		    log.info("To update the details of the booking by new id");

		    String newBookingId = reusableAPIs.createNewBooking();
		    log.info("new booking id" + newBookingId);
		    
		    log.info("validate updated firstname ");
			String firstname = getJsonKeysValue(update(newBookingId),"firstname");
			Assert.assertEquals(firstname, "JamesUpdate");
		}
		
	 /**
	    * In the below test case, We are passing existing booking entry 
	    * and updating the new entry by passing id
	    * validating by checking get booking by id should return updating values
	    */
		@Test(priority = 1)
		public void updateBookingWithExistingBookingID() {
		    log=extent.createTest("update booking with existing booking id");
		    log.info("To update the details of the booking by existing id");
	        String id = "2";
	        
			String firstname = getJsonKeysValue(update(id),"firstname");
			Assert.assertEquals(firstname, "JamesUpdate");
			
			String bookingDeatilsAfterPartialUpdate = reusableAPIs.getBookingDetailsById(id);
		    log.info("booking details after update" + bookingDeatilsAfterPartialUpdate);
		    
		    log.info("validate updated firstname ");
		    String firstnameFromGetapi = getJsonKeysValue(bookingDeatilsAfterPartialUpdate,"firstname");
			Assert.assertEquals(firstnameFromGetapi, "JamesUpdate");
		}
		
		/**
		    * In the below test case, We are passing invalid booking id 
		    * and trying to partially updating 
		    * validating by checking method not allowed response
		    */
		
		@Test(priority = 1)
		public void updateBookingForInvalidBookingID() {
		    log=extent.createTest("update booking with invalid booking id");
		    log.info("To update the details of the booking for invlaid booking id");
	        String invalidBookingId = "29999";
	        
	        String newAuthToken = reusableAPIs.getAuthToken();
		    log.info("auth token" + newAuthToken);
		    String cookieValue = "token="+newAuthToken;
		    
		    log.info("validate response for invalid booking id ");
			String testdata = returnJsonAsString(updateBookingJson);
			Response response = restAssuredUtils.putRequestwithHeaders(requestSpec, testdata,cookieValue, "/booking/"+invalidBookingId);
			response.then().assertThat().statusCode(405);
		}
		
		public String update(String bookingid) {
			String newAuthToken = reusableAPIs.getAuthToken();
		    log.info("auth token" + newAuthToken);
		    String cookieValue = "token="+newAuthToken;
		    
			String testdata = returnJsonAsString(updateBookingJson);
			Response response = restAssuredUtils.putRequestwithHeaders(requestSpec, testdata,cookieValue, "/booking/"+bookingid);
			response.then().spec(responseSpec);	
			log.info("Booking deatils after update by passing Booking ID :" + response.asString());
			
			return response.asString();
		}
	
}
