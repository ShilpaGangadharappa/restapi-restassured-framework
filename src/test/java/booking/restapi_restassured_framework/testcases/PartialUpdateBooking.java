package booking.restapi_restassured_framework.testcases;


import org.testng.Assert;
import org.testng.annotations.Test;

import booking.restapi_restassured_framework.reusable.ReusableAPIs;
import booking.restapi_restassured_framework.util.BaseClass;
import booking.restapi_restassured_framework.util.RestAssuredUtils;
import io.restassured.response.Response;

public class PartialUpdateBooking  extends BaseClass {
	
	RestAssuredUtils restAssuredUtils = new RestAssuredUtils();
	String partiallyUpdateBookingJson = System.getProperty("user.dir") + "\\testdata\\partiallyUpdateBooking.json";
	ReusableAPIs reusableAPIs = new ReusableAPIs();
	
	/**
     * In the below test case, We are creating new booking entry 
     * and partially updating the new entry by passing id
     * validating by checking get booking by id should return updated values
     */
	
    
   @Test
	public void partialUpdateBookingWithNewBookingID() {
	    log=extent.createTest("partial update booking with new booking id");
	    log.info("To partially update the details of the booking by new id");

	    String newBookingId = reusableAPIs.createNewBooking();
	    log.info("new booking id" + newBookingId);
	    
	    log.info("validate updated firstname ");
		String firstname = getJsonKeysValue(partialupdate(newBookingId),"firstname");
		Assert.assertEquals(firstname, "JamesPatch");
	}
	
   /**
    * In the below test case, We are passing existing booking entry 
    * and partially updating the new entry by passing id
    * validating by checking get booking by id should return updating values
    */
	@Test
	public void partialUpdateBookingWithExistingBookingID() {
	    log=extent.createTest("partial update booking with existing booking id");
	    log.info("To partially update the details of the booking by existing id");
        String id = "1";
        
		String firstname = getJsonKeysValue(partialupdate(id),"firstname");
		Assert.assertEquals(firstname, "JamesPatch");
		
		String bookingDeatilsAfterPartialUpdate = reusableAPIs.getBookingDetailsById(id);
	    log.info("booking details after partial update" + bookingDeatilsAfterPartialUpdate);
	    
	    log.info("validate updated firstname ");
	    String firstnameFromGetapi = getJsonKeysValue(bookingDeatilsAfterPartialUpdate,"firstname");
		Assert.assertEquals(firstnameFromGetapi, "JamesPatch");
	}
	
	/**
	    * In the below test case, We are passing invalid booking id 
	    * and trying to partially updating 
	    * validating by checking method not allowed response
	    */
	@Test
	public void partialUpdateBookingforInvalidBookingID() {
	    log=extent.createTest("partial update booking for invlaid booking id");
	    log.info("To partially update the details of the booking for invalid booking id");
        String invalidBookingId = "199999";
        
        String newAuthToken = reusableAPIs.getAuthToken();
	    log.info("auth token" + newAuthToken);
	    String cookieValue = "token="+newAuthToken;
	    
	    log.info("validate booking id not found response from invalid booking id");
		String testdata = returnJsonAsString(partiallyUpdateBookingJson);
		Response response = restAssuredUtils.patchRequestwithHeaders(requestSpec, testdata,cookieValue, "/booking/"+invalidBookingId);
		response.then().assertThat().statusCode(405);
	}
	
	/**
	    * In the below test case, We are passing bad request to update the booking deatils
	    * and validating by checking not found response
	    */
	@Test
	public void partialUpdateOfBookingwithBadRequest() {
	    log=extent.createTest("partial update of booking with put request");
	    log.info("To partially update the details with put request");
	    
	    String newBookingId = reusableAPIs.createNewBooking();
	    log.info("new booking id" + newBookingId);
        
        String newAuthToken = reusableAPIs.getAuthToken();
	    log.info("auth token" + newAuthToken);
	    String cookieValue = "token="+newAuthToken;
	    
	    log.info("validate Bad request for upadte booking id ");
		String testdata = returnJsonAsString(partiallyUpdateBookingJson);
		Response response = restAssuredUtils.putRequestwithHeaders(requestSpec, testdata,cookieValue, "/booking/"+newBookingId);
		response.then().assertThat().statusCode(400);
	}
	
	
	public String partialupdate(String bookingid) {
		String newAuthToken = reusableAPIs.getAuthToken();
	    log.info("auth token" + newAuthToken);
	    String cookieValue = "token="+newAuthToken;
	    
		String testdata = returnJsonAsString(partiallyUpdateBookingJson);
		Response response = restAssuredUtils.patchRequestwithHeaders(requestSpec, testdata,cookieValue, "/booking/"+bookingid);
		response.then().spec(responseSpec);	
		log.info("Booking deatils after partial update by passing Booking ID :" + response.asString());
		
		return response.asString();
	}
}
