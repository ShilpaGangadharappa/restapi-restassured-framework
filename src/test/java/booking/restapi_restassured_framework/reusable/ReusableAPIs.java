package booking.restapi_restassured_framework.reusable;

import booking.restapi_restassured_framework.util.BaseClass;
import booking.restapi_restassured_framework.util.RestAssuredUtils;
import io.restassured.response.Response;

public class ReusableAPIs extends BaseClass {
	RestAssuredUtils restAssuredUtils = new RestAssuredUtils();
	String authTokenJson = System.getProperty("user.dir") + "\\testdata\\authToken.json";
	String createBookingJson = System.getProperty("user.dir") + "\\testdata\\createBooking.json";


	public String getAuthToken() {
	    
		String testdata = returnJsonAsString(authTokenJson);
		Response response = restAssuredUtils.postRequest(requestSpec, testdata, "/auth");
		response.then().spec(responseSpec);			
		String token = response.then().extract().path("token");
		
		return token;
	}
	
	public String createNewBooking() {
		
		String testdata = returnJsonAsString(createBookingJson);
		Response response = restAssuredUtils.postRequest(requestSpec, testdata, "/booking");
		response.then().spec(responseSpec);	
		
		String newBookingID = response.then().extract().path("bookingid").toString();
		return newBookingID;
	}
	
	public String getBookingDetailsById(String id) {

		Response response = restAssuredUtils.getRequest(requestSpec, "/booking/"+ id);
		response.then().spec(responseSpec);	
		String bookingDetails = response.asString();
		return bookingDetails;
	}
	
	public Response getBookingDetailsByIdAndReturnResponse(String id) {
		
		 Response response =null;
		 return response = restAssuredUtils.getRequest(requestSpec, "/booking/"+ id);
		 
	}

}
