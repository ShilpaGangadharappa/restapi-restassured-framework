package booking.restapi_restassured_framework.util;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestAssuredUtils {
	
	/**  rest assured method for path request with heaters
	 * @param testdata, cookie ,endpoint
	 *  */
	public Response patchRequestwithHeaders(RequestSpecification requestSpec, String testData,String cookieValue,String endpoint) {

		try {
			Response response = given().
					spec(requestSpec).
					header("Content-Type", "application/json").
					header("Accept", "application/json").
					header("Cookie", cookieValue).
			        body(testData).log().body().
			    when().
					patch(endpoint);
			return response;

		} catch (Exception e) {

			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}

	}
	

	/**  rest assured method for put request with heaters
	 * @param testdata, cookie ,endpoint
	 * 
	 *  */
	public Response putRequestwithHeaders(RequestSpecification requestSpec, String testData,String cookieValue,String endpoint) {

		try {
			Response response = given().
					spec(requestSpec).
					header("Content-Type", "application/json").
					header("Accept", "application/json").
					header("Cookie", cookieValue).
			        body(testData).log().body().
			    when().
					put(endpoint);
			return response;

		} catch (Exception e) {

			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}

	}
	
	/**  rest assured method for post request with heaters
	 * @param testdata, cookie ,endpoint
	 * 
	 *  */
	public Response postRequest(RequestSpecification requestSpec, String testData, String endpoint) {

		try {
			Response response = given().
					spec(requestSpec).
					contentType("application/json").
		            body(testData).log().body().
		        when().
		        	post(endpoint);
			return response;

		} catch (Exception e) {

			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	/**  rest assured method for get request 
	 * @param endpoint
	 * 
	 *  */
	public Response getRequest(RequestSpecification requestSpec,String endpointurl) {

		try {

			Response response = given().
					spec(requestSpec).
				when().
					get(endpointurl);
			return response;
		}

		catch (Exception e) {

			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}

	}
	
	/**  rest assured method for delete details with heaters
	 * @param testdata, cookie ,endpoint
	 * 
	 *  */
	public Response deleteRequestwithHeaders(RequestSpecification requestSpec,String cookieValue,String endpoint) {

		try {
			Response response = given().
					spec(requestSpec).
					header("Content-Type", "application/json").
					header("Accept", "application/json").
					header("Cookie", cookieValue).
			        log().body().
			    when().
			        delete(endpoint);
			return response;

		} catch (Exception e) {

			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}

	}
	

}
