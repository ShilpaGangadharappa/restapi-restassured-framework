package booking.restapi_restassured_framework.util;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.simple.JSONObject;
import org.json.simple.parser.*;

public class FrameWorkUtils {
	
	private Logger log = LoggerFactory.getLogger(FrameWorkUtils.class);
	
	protected static Properties properties;
	public static String PROPERTIES_PATH = System.getProperty("user.dir") + File.separator + "properties/" + File.separator + "Booking.properties";

	/*******************************************************
	 * @param properties file keys
	 * @returns values from properties file
	 ******************************************************/
	public static String readConfigurationFile(String key) {
		try{
			properties = new Properties();
			properties.load(new FileInputStream(PROPERTIES_PATH));
			
		} catch (Exception e){
			System.out.println("Cannot find key: "+key+" in Config file due to exception : "+e);
		}
		return properties.getProperty(key).trim();	
	}
	

	/*******************************************************
	 * @param jsonpath
	 * @returns Json string
	 ******************************************************/
	public String returnJsonAsString(String jsonPath) {
		InputStream fis;
		BufferedInputStream bis;
		DataInputStream dis;
		String jsonData = "";
		try {
			File initialFile = new File(jsonPath);
			fis = new FileInputStream(initialFile);

			bis = new BufferedInputStream(fis);
			dis = new DataInputStream(bis);
			while (dis.available() != 0) {
				jsonData = jsonData + dis.readLine().trim();
			}
			dis.close();
			bis.close();
			fis.close();
		} catch (IOException ioexception) {
			log.error("User is getting this exception" + ioexception.getMessage()
					+ " in reading json from this " + jsonPath);

		} catch (Exception e) {

		}

		return jsonData;
	}

	/*******************************************************
	 * @param json, key
	 * @returns values
	 ******************************************************/
	public String getJsonKeysValue(String json, String key) {
		
		JsonObject jsonObject =  new  JsonParser().parse(json).getAsJsonObject();

        String value =jsonObject.get(key).getAsString();
		
		return value;
	}
}
