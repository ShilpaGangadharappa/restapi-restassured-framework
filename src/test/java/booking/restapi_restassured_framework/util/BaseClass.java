package booking.restapi_restassured_framework.util;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;


public class BaseClass extends FrameWorkUtils{
	public static ExtentReports extent;
	public static ExtentTest log;
	protected static RequestSpecification requestSpec;
	protected static ResponseSpecification responseSpec;
	
	/**
	 * setting base URL and report path
	 * */
	@BeforeSuite
	public void setup()
	{
		requestSpec = new RequestSpecBuilder().
        		setBaseUri(readConfigurationFile("baseUrl")).
        		build();
	    ExtentHtmlReporter reporter=new ExtentHtmlReporter("./Reports/Report.html");
	    extent = new ExtentReports();
	    extent.attachReporter(reporter);
	  }

	/**
	 * validating status of response
	 * */
	@BeforeMethod
	public void beforeMethod() {
    	responseSpec = new ResponseSpecBuilder().expectStatusCode(200).build();        
	}
	@AfterSuite
	public void tearDown()
	{
		extent.flush();
	}
}
